<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if ($arResult["isFormErrors"] == "Y"):?><?=$arResult["FORM_ERRORS_TEXT"];?><?endif;?>
<?=$arResult["FORM_NOTE"]?>
<?if ($arResult["isFormNote"] != "Y")
{
    ?>
        <?= str_replace('<form','<form method="post" class="js-validated-form"',$arResult['FORM_HEADER']); ?>
    <form method="post" class="js-validated-form">

        <div class="popup-feedback__input-cover"> <label for="" class="popup-feedback__input-label">Ваше имя и фамилия:</label> <input type="text" class="popup-feedback__input js-validated-field" name="form_text_26" value="dw" data-validated_name="name" /></div>

        <div class="popup-feedback__double-column">
            <div class="popup-feedback__input-cover"> <label for="" class="popup-feedback__input-label">Телефон</label> <input type="text" class="popup-feedback__input mask-phone-js js-validated-field" name="form_text_27" value="" data-validated_name="phone" /> </div>

            <div class="popup-feedback__input-cover"> <label for="" class="popup-feedback__input-label">Email</label> <input type="email" class="popup-feedback__input js-validated-field" data-validated_name="mail" name="form_email_28" value="" size="0" /> </div>
        </div>

        <div class="popup-feedback__input-cover"> <label for="" class="popup-feedback__input-label">Название компании:</label> <input type="text" class="popup-feedback__input" name="form_text_29" value="" /> </div>

        <div class="popup-feedback__input-cover"> <label for="" class="popup-feedback__input-label">Опишите Вашу задачу:</label> <input type="text" class="popup-feedback__textarea" name="form_text_30" value="" /> </div>

        <div class="popup-feedback__consent">
            <div class="popup-feedback__consent-form-wrapper"> <input type="checkbox" class="popup-feedback__consent-input" checked="checked" id="15" name="form_checkbox_SIMPLE_QUESTION_749[]" value="31" /> <label class="popup-feedback__consent-form" for="15"> <a href="./article-data-processing.html" target="_blank" > Нажимая кнопку &laquo;Отправить&raquo;, я даю свое согласие на обработку моих персональных данных, в соответствии с Федеральным законом от 27.07.2006 года №152-ФЗ «О персональных данных», на условиях и для целей, определенных в Согласии на обработку персональных данных. </a> </label> </div>

            <div class="popup-feedback__consent-form-wrapper"> <input type="checkbox" checked="checked" class="popup-feedback__consent-input" id="16" name="form_checkbox_SIMPLE_QUESTION_851[]" value="32" /> <label class="popup-feedback__consent-form" for="16"> <a href="./article-privacy-policy.html" target="_blank" > Оставляя данные на Сайте, заполняя регистрационную форму, Вы соглашаетесь с настоящей Политикой конфиденциальности. </a> </label> </div>
        </div>

        <div class="popup-feedback__button-cover">
            <input  class="button button_modal-gold js-button-submit" <?=(intval($arResult["F_RIGHT"]) < 10 ? "disabled=\"disabled\"" : "");?> type="submit" name="web_form_submit" value="<?=htmlspecialcharsbx(trim($arResult["arForm"]["BUTTON"]) == '' ? GetMessage("FORM_ADD") : $arResult["arForm"]["BUTTON"]);?>" />

    </form>
    <?php
} //endif (isFormNote)