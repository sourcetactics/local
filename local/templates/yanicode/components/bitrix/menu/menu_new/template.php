<?php echo 111;
 if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
?>

<?php
 if(empty($arResult["ALL_ITEMS"])) return;
?>
<div class="header-nav">
    <nav class="nav-list">
    <?php
        foreach($arResult['ALL_ITEMS'] as $item){
            echo '<a href="'.$item['LINK'] .'" class="nav-list__item">'.$item['TEXT'] .'</a>';
        }
    ?>

</nav>
<div class="header__phone">
    <a href="tel:+79114510616">+79114510616</a>
</div>
</div>